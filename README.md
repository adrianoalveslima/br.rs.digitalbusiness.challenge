### Java Spring template project

Foi utilizado Docker no projeto. Portanto, para executá-lo, deve-se instalar o Docker, acessar a pasta do projeto e executar os seguintes comandos: **docker-compose build** e **docker-compose up**. Para carregar a base a partir dos arquivos csv, acesse o endereço: **http://localhost:8080/index**. Obs.: O carregamento demora aproximadamente 1 minuto, aguardar o carregamento da página.

### Perguntas e Respostas

As perguntas e as respectivas respostas do desafio estão salvas nos arquivos pdf com nomes "Perguntas e Respostas-x.pdf".
