package br.rs.digitalbusiness.challenge.model;

import java.util.ArrayList;
import java.util.List;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class AuthorsField extends AbstractBeanField<List<Author>, String> {

    @Override
    protected List<Author> convert(String value) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        List<Author> authors = null;
        if (!value.isEmpty() && !value.equals("[]")) {
            authors = new ArrayList<Author>();
            String[] ids = value.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
            for (String id : ids) {
                Author author = new Author();
                author.setId(Long.parseLong(id));
                authors.add(author);
            }
        }
        return authors;
    }

}
