package br.rs.digitalbusiness.challenge.model;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class FormatField extends AbstractBeanField<Format, Long> {

    @Override
    protected Format convert(String value) throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        Format format = null;
        if (!value.isEmpty()) {
            format = new Format();
            format.setId(Long.parseLong(value));
        }
        return format;
    }

}
