package br.rs.digitalbusiness.challenge.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.opencsv.bean.CsvBindByPosition;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "categories", schema = "public")
public class Category {
    @Id
    @CsvBindByPosition(position = 0)
    @Column(name = "category_id")
    private Long id;

    @CsvBindByPosition(position = 1)
    @Column(name = "name")
    @Getter
    @Setter
    private String name;

    @ManyToMany(mappedBy = "categories")
    private List<Book> books;

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
