package br.rs.digitalbusiness.challenge.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.opencsv.bean.CsvBindAndSplitByName;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;
import com.opencsv.bean.CsvDate;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "books", schema = "public")
public class Book {
    @Id
    @CsvBindByName
    @Column(name = "book_id")
    @Getter
    @Setter
    private Long id;

    @CsvBindByName
    @Column(name = "description", length = 10000)
    @Getter
    @Setter
    private String description;

    @CsvBindByName(column = "bestsellers-rank")
    @Column(name = "rank")
    @Getter
    @Setter
    private Long rank;

    @CsvBindByName(column = "dimension-x")
    @Column(name = "dimension_x")
    @Getter
    @Setter
    private Double dimensionX;

    @CsvBindByName(column = "dimension-y")
    @Column(name = "dimension_y")
    @Getter
    @Setter
    private Double dimensionY;

    @CsvBindByName(column = "dimension-z")
    @Column(name = "dimension_z")
    @Getter
    @Setter
    private Double dimensionZ;

    @CsvBindByName
    @Column(name = "edition")
    @Getter
    @Setter
    private String edition;

    @CsvBindByName(column = "edition-statement")
    @Column(name = "edition_statement")
    @Getter
    @Setter
    private String editionStatement;

    @CsvBindByName(column = "illustrations-note")
    @Column(name = "illustrations_note")
    @Getter
    @Setter
    private String illustrationsNote;

    @CsvBindByName(column = "image-checksum")
    @Column(name = "image_checksum")
    @Getter
    @Setter
    private String imageCheckSum;

    @CsvBindByName(column = "image-path")
    @Column(name = "image_path")
    @Getter
    @Setter
    private String imagePath;

    @CsvBindByName(column = "image-url")
    @Column(name = "image_url")
    @Getter
    @Setter
    private String imageUrl;

    @CsvBindByName(column = "imprint")
    @Column(name = "imprint")
    @Getter
    @Setter
    private String imprint;

    @CsvBindByName(column = "isbn10")
    @Column(name = "isbn10")
    @Getter
    @Setter
    private String isbn10;

    @CsvBindByName(column = "isbn13")
    @Column(name = "isbn13")
    @Getter
    @Setter
    private Long isbn13;

    @CsvBindByName(column = "lang")
    @Column(name = "lang")
    @Getter
    @Setter
    private String lang;

    @CsvDate(value = "yyyy-MM-dd hh:mm:ss")
    @CsvBindByName(column = "publication-date")
    @Column(name = "publication_date")
    @Getter
    @Setter
    private Date publicationDate;

    @CsvBindByName(column = "rating-avg")
    @Column(name = "rating_avg")
    @Getter
    @Setter
    private Double ratingAvg;

    @CsvBindByName(column = "rating-count")
    @Column(name = "rating_count")
    @Getter
    @Setter
    private Long ratingCount;

    @CsvBindByName(column = "title")
    @Column(name = "title", length = 500)
    @Getter
    @Setter
    private String title;

    @CsvBindByName(column = "url")
    @Column(name = "url")
    @Getter
    @Setter
    private String url;

    @CsvBindByName(column = "weight")
    @Column(name = "weight")
    @Getter
    @Setter
    private Double weight;

    @CsvCustomBindByName(column = "format", converter = FormatField.class)
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "format_id", columnDefinition = "bigint")
    private Format format;

    @CsvCustomBindByName(column = "authors", converter = AuthorsField.class)
    @ManyToMany
    @JoinTable(name = "authors_books", joinColumns = @JoinColumn(name = "book_id"), inverseJoinColumns = @JoinColumn(name = "author_id"))
    private List<Author> authors;

    @CsvCustomBindByName(column = "categories", converter = CategoriesField.class)
    @ManyToMany
    @JoinTable(name = "categories_books", joinColumns = @JoinColumn(name = "book_id"), inverseJoinColumns = @JoinColumn(name = "category_id"))
    private List<Category> categories;

    public Format getFormat() {
        return format;
    }

    public void setFormat(Format format) {
        this.format = format;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
