package br.rs.digitalbusiness.challenge.model;

import java.util.ArrayList;
import java.util.List;

import com.opencsv.bean.AbstractBeanField;
import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;

public class CategoriesField extends AbstractBeanField<List<Category>, String> {

    @Override
    protected List<Category> convert(String value)
            throws CsvDataTypeMismatchException, CsvConstraintViolationException {
        List<Category> categories = null;
        if (!value.isEmpty() && !value.equals("[]")) {
            categories = new ArrayList<Category>();
            String[] ids = value.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\s", "").split(",");
            for (String id : ids) {
                Category category = new Category();
                category.setId(Long.parseLong(id));
                categories.add(category);
            }
        }
        return categories;
    }

}
