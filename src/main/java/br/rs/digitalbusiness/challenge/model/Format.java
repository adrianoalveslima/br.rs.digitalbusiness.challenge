package br.rs.digitalbusiness.challenge.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.opencsv.bean.CsvBindByPosition;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "formats", schema = "public")
public class Format {
    @Id
    @CsvBindByPosition(position = 0)
    @Column(name = "format_id")
    private Long id;

    @CsvBindByPosition(position = 1)
    @Column(name = "name")
    @Getter
    @Setter
    private String name;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

}
