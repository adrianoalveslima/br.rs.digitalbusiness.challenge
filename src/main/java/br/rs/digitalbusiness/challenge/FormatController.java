package br.rs.digitalbusiness.challenge;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.rs.digitalbusiness.challenge.model.Format;
import br.rs.digitalbusiness.challenge.repository.FormatRepository;

@Controller
@RequestMapping("formats")
public class FormatController {

    @Autowired
    private FormatRepository repository;

    @RequestMapping("list")
    public String listFormats(Model model) throws IllegalStateException, IOException {
        Iterable<Format> formatsDb = repository.findAll();
        if (model != null)
            model.addAttribute("formats", formatsDb);
        return "listformats";
    }
}
