package br.rs.digitalbusiness.challenge;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.stream.StreamSupport;

import org.apache.tomcat.util.http.fileupload.IOUtils;

import br.rs.digitalbusiness.challenge.model.Author;
import br.rs.digitalbusiness.challenge.model.Book;
import br.rs.digitalbusiness.challenge.model.Category;
import br.rs.digitalbusiness.challenge.model.Format;
import br.rs.digitalbusiness.challenge.repository.AuthorRepository;
import br.rs.digitalbusiness.challenge.repository.BookRepository;
import br.rs.digitalbusiness.challenge.repository.CategoryRepository;
import br.rs.digitalbusiness.challenge.repository.FormatRepository;

import com.opencsv.bean.CsvToBeanBuilder;

@Controller
@RequestMapping("index")
public class IndexController {

    @Autowired
    private FormatRepository repositoryFormat;

    @Autowired
    private AuthorRepository repositoryAuthor;

    @Autowired
    private CategoryRepository repositoryCategory;

    @Autowired
    private BookRepository repositoryBook;

    @RequestMapping("/")
    public String index(Model model) throws IllegalStateException, IOException {
        loadFormats();
        loadAuthors();
        loadCategories();
        loadBooks();

        return "index";
    }

    private void loadCategories() throws FileNotFoundException, IOException {
        Iterable<Category> categoriesDb = repositoryCategory.findAll();
        if (StreamSupport.stream(categoriesDb.spliterator(), false).count() == 0) {
            File file = new File("categories.csv");
            IOUtils.copy(new ClassPathResource("Teste-DataEngineer-Bases/categories.csv").getInputStream(),
                    new FileOutputStream(file));

            List<Category> categories = new CsvToBeanBuilder(new FileReader(file)).withType(Category.class).build()
                    .parse();
            repositoryCategory.saveAll(categories);
        }
    }

    private void loadAuthors() throws FileNotFoundException, IOException {
        Iterable<Author> authorsDb = repositoryAuthor.findAll();
        if (StreamSupport.stream(authorsDb.spliterator(), false).count() == 0) {
            File file = new File("authors.csv");
            IOUtils.copy(new ClassPathResource("Teste-DataEngineer-Bases/authors.csv").getInputStream(),
                    new FileOutputStream(file));

            List<Author> authors = new CsvToBeanBuilder(new FileReader(file)).withType(Author.class).build().parse();
            repositoryAuthor.saveAll(authors);
        }
    }

    private void loadFormats() throws FileNotFoundException, IOException {
        Iterable<Format> formatsDb = repositoryFormat.findAll();
        if (StreamSupport.stream(formatsDb.spliterator(), false).count() == 0) {
            File file = new File("formats.csv");
            IOUtils.copy(new ClassPathResource("Teste-DataEngineer-Bases/formats.csv").getInputStream(),
                    new FileOutputStream(file));
            List<Format> formats = new CsvToBeanBuilder(new FileReader(file)).withType(Format.class).build().parse();
            repositoryFormat.saveAll(formats);
        }
    }

    private void loadBooks() throws FileNotFoundException, IOException {
        Iterable<Book> booksDb = repositoryBook.findAll();
        if (StreamSupport.stream(booksDb.spliterator(), false).count() == 0) {
            File file = new File("dataset.csv");
            IOUtils.copy(new ClassPathResource("Teste-DataEngineer-Bases/dataset.csv").getInputStream(),
                    new FileOutputStream(file));

            List<Book> books = new CsvToBeanBuilder(new FileReader(file)).withType(Book.class).build().parse();
            Iterable<Format> formats = repositoryFormat.findAll();
            Iterable<Author> authors = repositoryAuthor.findAll();
            Iterable<Category> categories = repositoryCategory.findAll();
            for (Book book : books) {
                if (book.getFormat() != null && book.getFormat().getId() != null) {
                    for (Format format : formats) {
                        if (format.getId().equals(book.getFormat().getId()))
                            book.setFormat(format);
                    }
                }

                if (book.getAuthors() != null) {
                    List<Author> listAuthorsBook = new ArrayList<Author>();
                    for (Author authorBook : book.getAuthors()) {
                        Author author = StreamSupport.stream(authors.spliterator(), false)
                                .filter(a -> a.getId().equals(authorBook.getId())).findAny().get();
                        listAuthorsBook.add(author);
                    }
                    book.setAuthors(listAuthorsBook);
                }

                if (book.getCategories() != null) {
                    List<Category> listCategoriesBook = new ArrayList<Category>();
                    for (Category categoryBook : book.getCategories()) {
                        Category category = StreamSupport.stream(categories.spliterator(), false)
                                .filter(c -> c.getId().equals(categoryBook.getId())).findAny().get();
                        listCategoriesBook.add(category);
                    }
                    book.setCategories(listCategoriesBook);
                }
            }

            repositoryBook.saveAll(books);
        }
    }

}