package br.rs.digitalbusiness.challenge;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.rs.digitalbusiness.challenge.repository.BookRepository;
import br.rs.digitalbusiness.challenge.model.Book;

@Controller
@RequestMapping("books")
public class BookController {

    @Autowired
    private BookRepository repository;

    @RequestMapping("list")
    public String listBooks(Model model) throws IllegalStateException, IOException {
        Iterable<Book> booksDb = repository.findAll();
        if (model != null)
            model.addAttribute("books", booksDb);
        return "listbooks";
    }

    @RequestMapping("listquestions")
    public String listQuestions(Model model) throws IllegalStateException, IOException {
        model.addAttribute("qtdeBooks", Long.parseLong(String.valueOf(repository.qtdeBooks())));
        model.addAttribute("authorsWithMoreBooks", repository.authorsWithMoreBooks());
        model.addAttribute("qtdeBooksWithOneAuthor",
                Long.parseLong(String.valueOf(repository.qtdeBooksWithOneAuthor())));
        model.addAttribute("qtdeBooksByCategory", repository.qtdeBooksByCategory());
        model.addAttribute("categoriesWithMoreBooks", repository.categoriesWithMoreBooks());
        model.addAttribute("formatWithMoreBooks", repository.formatWithMoreBooks());
        model.addAttribute("bestSellers", repository.bestSellers());
        model.addAttribute("bestAvg", repository.bestAvg());
        model.addAttribute("qtdeAvgMoreThanThree", Long.parseLong(String.valueOf(repository.qtdeAvgMoreThanThree())));
        model.addAttribute("qtdeBooksPublicationDate",
                Long.parseLong(String.valueOf(repository.qtdeBooksPublicationDate())));
        return "listquestions";
    }

}
