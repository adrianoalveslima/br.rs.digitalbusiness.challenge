package br.rs.digitalbusiness.challenge;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.rs.digitalbusiness.challenge.model.Author;
import br.rs.digitalbusiness.challenge.repository.AuthorRepository;

@Controller
@RequestMapping("authors")
public class AuthorController {

    @Autowired
    private AuthorRepository repository;

    @RequestMapping("list")
    public String listAuthors(Model model) throws IllegalStateException, IOException {
        Iterable<Author> authorsDb = repository.findAll();
        if (model != null)
            model.addAttribute("authors", authorsDb);
        return "listauthors";
    }
}
