package br.rs.digitalbusiness.challenge.repository;

import org.springframework.data.repository.CrudRepository;

import br.rs.digitalbusiness.challenge.model.Format;

public interface FormatRepository extends CrudRepository<Format, Long> {

}
