package br.rs.digitalbusiness.challenge.repository;

import org.springframework.data.repository.CrudRepository;

import br.rs.digitalbusiness.challenge.model.Author;

public interface AuthorRepository extends CrudRepository<Author, Long> {

}
