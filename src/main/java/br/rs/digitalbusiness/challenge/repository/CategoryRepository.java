package br.rs.digitalbusiness.challenge.repository;

import org.springframework.data.repository.CrudRepository;

import br.rs.digitalbusiness.challenge.model.Category;

public interface CategoryRepository extends CrudRepository<Category, Long> {

}
