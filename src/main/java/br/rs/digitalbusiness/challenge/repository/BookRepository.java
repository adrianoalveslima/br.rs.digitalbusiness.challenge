package br.rs.digitalbusiness.challenge.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.rs.digitalbusiness.challenge.model.Author;
import br.rs.digitalbusiness.challenge.model.Book;

public interface BookRepository extends CrudRepository<Book, Long> {

        @Query(value = "SELECT count(book_id) FROM public.books", nativeQuery = true)
        Object qtdeBooks();

        @Query(value = "SELECT count(book_id) FROM public.books b "
                        + " where b.book_id in(select ab.book_id from authors_books ab "
                        + " inner join authors a on a.author_id = ab.author_id "
                        + " group by ab.book_id having count(a.author_id) = 1)", nativeQuery = true)
        Object qtdeBooksWithOneAuthor();

        @Query(value = "SELECT aut.author_id, aut.name, count(ab.book_id) as qtde FROM public.authors aut "
                        + "inner join authors_books ab on ab.author_id = aut.author_id "
                        + "inner join books b on ab.book_id = b.book_id group by aut.author_id, aut.name order by qtde desc limit 5", nativeQuery = true)
        List<Object[]> authorsWithMoreBooks();

        @Query(value = "SELECT c.name, count(b.book_id) as qtde FROM books b "
                        + "inner join categories_books cb on cb.book_id = b.book_id "
                        + "inner join categories c on cb.category_id = c.category_id group by c.name", nativeQuery = true)
        List<Object[]> qtdeBooksByCategory();

        @Query(value = "SELECT c.name, count(b.book_id) as qtde FROM books b "
                        + "inner join categories_books cb on cb.book_id = b.book_id "
                        + "inner join categories c on cb.category_id = c.category_id group by c.name order by qtde desc limit 5", nativeQuery = true)
        List<Object[]> categoriesWithMoreBooks();

        @Query(value = "SELECT f.name, count(b.book_id) as qtde FROM formats f "
                        + "inner join books b on b.format_id = f.format_id group by f.name order by qtde desc limit 1", nativeQuery = true)
        List<Object[]> formatWithMoreBooks();

        @Query(value = "SELECT b.title FROM books b order by rank desc limit 10", nativeQuery = true)
        List<Object> bestSellers();

        @Query(value = "SELECT b.title FROM books b order by rating_avg desc limit 10", nativeQuery = true)
        List<Object> bestAvg();

        @Query(value = "SELECT count(book_id) FROM public.books b where b.book_id in(select ab.book_id from books ab "
                        + "group by ab.book_id having ab.rating_avg > 3.5)", nativeQuery = true)
        Object qtdeAvgMoreThanThree();

        @Query(value = "SELECT count(book_id) FROM public.books b where b.publication_date > '2020-01-01'", nativeQuery = true)
        Object qtdeBooksPublicationDate();
}
