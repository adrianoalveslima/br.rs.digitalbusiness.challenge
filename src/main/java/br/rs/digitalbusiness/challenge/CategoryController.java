package br.rs.digitalbusiness.challenge;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.rs.digitalbusiness.challenge.model.Category;
import br.rs.digitalbusiness.challenge.repository.CategoryRepository;

@Controller
@RequestMapping("categories")
public class CategoryController {

    @Autowired
    private CategoryRepository repository;

    @RequestMapping("list")
    public String listCategories(Model model) throws IllegalStateException, IOException {
        Iterable<Category> categoriesDb = repository.findAll();

        if (model != null)
            model.addAttribute("categories", categoriesDb);
        return "listcategories";
    }
}
